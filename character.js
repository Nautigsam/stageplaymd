'use strict'

const CHARACTER_MARKER = '@'.charCodeAt(0)

module.exports = (state, startLine, endLine, silent) => {
  const start = state.bMarks[startLine] + state.tShift[startLine]
  const max = state.eMarks[startLine]

  if (CHARACTER_MARKER !== state.src.charCodeAt(start)) {
    return false
  } else if (silent) {
    return true
  }
  
  const text = state.src.substring(start, max)
  const characterName = text.substring(1)

  state.line = startLine + 1

  let token = state.push('character_open', 'div', 1)
  token.attrs = [['class', 'character']]
  token.markup = CHARACTER_MARKER
  token.map = [startLine, state.line]

  token = state.push('inline', '', 0)
  token.meta = {isCharacter: true}
  token.content = characterName
  token.map = [startLine, state.line - 1]
  token.children = []

  token = state.push('character_close', 'div', -1)
  token.markup = CHARACTER_MARKER

  return true
}
