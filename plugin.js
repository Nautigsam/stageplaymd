'use strict'

function *getCharactersIndexes(tokens, start) {
  for (let i = start; i >= 0; i--) {
    const curToken = tokens[i]
    if (curToken.type === 'character_close') {
      yield i
    }
  }
}

module.exports = function (md) {
  md.block.ruler.before('paragraph', 'character', require('./character'))
  md.block.ruler.before('paragraph', 'setting', require('./setting'))
  md.inline.ruler.before('emphasis', 'stage_directions', require('./stage-directions').tokenize);
  md.inline.ruler2.before('emphasis', 'stage_directions', require('./stage-directions').postProcess);
  //md.core.ruler.push('verse_padding', require('./verse-padding'))

  const padding_re = /^\|\s*(.+)/ 
  md.renderer.rules['text'] = (tokens, idx, _options, env, slf) => {
    const token = tokens[idx]
    const res = padding_re.exec(token.content)
    if (Array.isArray(res)) {
      const [secondCharacter, firstCharacter] = [...getCharactersIndexes(tokens, idx - 1)]
      const test = [...getCharactersIndexes(tokens, idx - 1)]
      console.log('test: ', test)
      if (firstCharacter) {
        // Get all text between current token and first character
        const text = tokens
          .slice(firstCharacter + 1, idx)
          .filter(t => t.type === 'text')
          .reduce((acc, t) => `${acc}${t.content}`, '')
        console.log(text)
      }
    }
    return slf.renderToken(tokens, idx, _options, env)
  }

  md.renderer.rules['heading_open'] = (tokens, idx, _options, env, slf) => {
    const token = tokens[idx]
    switch(token.tag) {
      case 'h1':
        token.attrJoin('class', 'act')
        break
      case 'h2':
        token.attrJoin('class', 'scene')
        break
    }
    return slf.renderToken(tokens, idx, _options, env)
  }

  md.renderer.rules['blockquote_open'] = (tokens, idx, _options, env, slf) => {
    tokens[idx].attrJoin('class', 'blackout')
    return slf.renderToken(tokens, idx, _options, env)
  }
}
