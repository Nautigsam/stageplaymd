'use strict'

const DIRECTIONS_CHAR_STR = '-'
const DIRECTIONS_CHAR = DIRECTIONS_CHAR_STR.charCodeAt(0)
const TWO_DIRECTIONS_CHAR_STR = [,,].fill(DIRECTIONS_CHAR_STR).join('')

module.exports = {
  // Insert each marker as a separate text token, and add it to delimiter list
  tokenize(state, silent) {
    let start = state.pos
    const marker = state.src.charCodeAt(start)

    if (silent || marker !== DIRECTIONS_CHAR) { return false }

    let scanned = state.scanDelims(start, true)
    let len = scanned.length

    if (len < 2) { return false }

    if (len % 2) {
      const token = state.push('text', '', 0)
      token.content = DIRECTIONS_CHAR_STR
      len--
    }

    for (let i=0; i<len; i+=2) {
      const token = state.push('text', '', 0)
      token.content = TWO_DIRECTIONS_CHAR_STR

      state.delimiters.push({
        marker,
        jump: i,
        token: state.tokens.length - 1,
        level: state.level,
        end: -1,
        open: scanned.can_open,
        close: scanned.can_close
      })
    }

    state.pos += len
    return true
  },
  // Walk through delimiter list and replace text tokens with tags
  postProcess(state) {
    const delimiters = state.delimiters
    const max = state.delimiters.length
    const loneMarkers = []

    for (let i=0; i<max; i++) {
      const startDelim = delimiters[i]

      if (startDelim.marker !== DIRECTIONS_CHAR) {
        continue
      }

      if (startDelim.end === -1) {
        continue
      }

      const endDelim = delimiters[startDelim.end]

      let token = state.tokens[startDelim.token]
      token.type = 'stage_directions_open'
      token.tag = 'span'
      token.attrs = [['class', 'stage_directions']]
      token.nesting = 1
      token.markup = TWO_DIRECTIONS_CHAR_STR
      token.content = ''

      token = state.tokens[endDelim.token]
      token.type = 'stage_directions_close'
      token.tag = 'span'
      token.nesting = -1
      token.markup = TWO_DIRECTIONS_CHAR_STR
      token.content = ''

      if (state.tokens[endDelim.token - 1].type === 'text' &&
          state.tokens[endDelim.token - 1].content === DIRECTIONS_CHAR_STR) {
        loneMarkers.push(endDelim.token - 1)
      }
    }

    while (loneMarkers.length) {
      const i = loneMarkers.pop()
      let j = i + 1

      while (j<state.tokens.length && state.tokens[j].type === 'mark_close') {
        j++
      }

      j--

      if (i !== j) {
        token = state.tokens[j]
        state.tokens[j] = state.tokens[i]
        state.tokens[i] = token
      }
    }
  }
}
