'use strict'

const fs = require('fs').promises
const path = require('path')

const mdit = require('markdown-it')()
const pug = require('pug')

mdit
  .use(require('./plugin'))

const argv = require('yargs')
  .option('input', {
    alias: 'i',
    type: 'string'
  })
  .option('output', {
    alias: 'o',
    type: 'string'
  })
  .option('print', {
    alias: 'p',
    type: 'boolean'
  })
  .demandOption(['i', 'o'])
  .argv

async function main() {
  const md = await fs.readFile(path.resolve(__dirname, argv.input), 'utf8')
  const html = mdit.render(md)
  if (argv.print) {
    console.log(html)
  }
  await fs.writeFile(path.resolve(__dirname, 'dist/out.html'), html, 'utf8')
  const output = pug.renderFile(path.resolve(__dirname, 'dist/index.pug'))
  await fs.writeFile(path.resolve(__dirname, `dist/${argv.output}`), output, 'utf8')
}

main()
